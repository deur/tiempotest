<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegistroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('registro')->insert([
        	'nombre'=>'Alejandra',
        	'apellido'=>'Velez',
        	'identificacion_id'=>'2',
        	'identificacion'=>'928383',
        	'telefono'=>'2211',
        	'universidad'=>'Antonio Nariño',
        	'ocupacion_id'=>'7',
        	'email'=>'ale@gmail.com',
        	'refrigerio1'=>'1',
        	'refrigerio2'=>'1',
        	'asistencia'=>'1',
        	'departamento_id'=>'3',
        	'ciudad_id'=>'7',
        ]);
    }
}
