@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
	
			<!--Si no tengo errores se cuentan, es decir que si hay mas de 0 errores asigno una alerta-->
			@if(count($errors)>0)

			<!--Voy a mostrar las alertas de las validaciones propuestas en  -->
			<div class="alert alert-danger">
				<ul>
					<!--Bucle que recibe todos los errores de las validaciones y que se va a listar -->
					@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
					@endforeach
				</ul>
			</div>
			@endif
            <!--Luego de hacer las validaciones si enviar los parametros //home.update(home hace el llamado a la ruta, update a la clase del controlador de la ruta)-->
            {!!Form::model($tarea,['method'=>'PATCH','route'=>['home.update',$tarea->id],'files'=>'true'])!!}
			{{Form::token()}}
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="nombre">Nombre la tarea</label>
					<input type="text" name="nombre" required value="{{$tarea->nombre}}" class="form-control">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="categoria_id">Seleccione Tipo de Identificación</label>
					<select name="categoria_id" class="selectpicker col-lg-12 col-md-12 col-sm-12 col-xs-12" data-live-search="true">
						@foreach($categorias as $cat)
                        @if ($cat->id_categoria==$tarea->categoria_id)
						<option value="{{$cat->id_categoria}}" selected >{{$cat->nombre}}</option>
						@else
						<option value="{{$cat->id_categoria}}">{{$cat->nombre}}</option>
						@endif   
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="descripcion">Descripcion de la tarea</label>
					<input type="text" name="descripcion" value="{{$tarea->descripcion}}" class="form-control" >
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="fecha_inicio">Fecha de inicio</label>
					<input type="date" name="fecha_inicio" value="{{$tarea->fecha_inicio}}" class="form-control">
				</div>
			</div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="estado">Seleccione el estado de su tarea</label><br>
					<select name="estado" class="selectpicker col-lg-12 col-md-12 col-sm-12 col-xs-12" data-live-search="true">
                            @if($tarea->estado=="$tarea->estado")
                            <option  value="{{$tarea->estado}}">{{$tarea->estado}}</option>
                            <option  value="Ejecutandose">Ejecutandose</option>
							<option  value="Pospuesta">Pospuesta</option>
                            <option  value="Finalizada">Finalizada</option>
                            @endif

					</select>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="fecha_finalizacion">Fecha de finalizacion</label>
					<input type="date" name="fecha_finalizacion" value="{{$tarea->fecha_finalizacion}}" class="form-control">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button><br><br>
				<a class="btn" href="/home">Regresar</a>
			</div>
			</div>
			{!!Form::close()!!}

		</div>



	</div>
	</div>
@endsection
