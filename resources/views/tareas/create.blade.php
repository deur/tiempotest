@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
	
			<!--Si no tengo errores se cuentan, es decir que si hay mas de 0 errores asigno una alerta-->
			@if(count($errors)>0)
			<!--Voy a mostrar las alertas de las validaciones propuestas en CategoriaFormRequest -->
			<div class="alert alert-danger">
				<ul>
					<!--Bucle que recibe todos los errores de las validaciones y que se va a listar -->
					@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
					@endforeach
				</ul>
			</div>
			@endif
			<!--Luego de hacer las validaciones si enviar los parametros-->
			{!!Form::open(array('url'=>'home','method'=>'POST','autocomplete'=>'off'))!!}
			{{Form::token()}}
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="nombre">Nombre la tarea</label>
					<input type="text" name="nombre"  class="form-control" placeholder="Nombre...">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="categoria_id">Seleccione la categoría de su tarea</label><br>
					<select name="categoria_id" class="selectpicker col-lg-12 col-md-12 col-sm-12 col-xs-12" data-live-search="true">
						@foreach($categorias as $cat)
							<option   value="{{$cat->id_categoria}}" >{{$cat->nombre}}</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="descripcion">Descripcion de la tarea</label>
					<input type="text" name="descripcion"  class="form-control" placeholder="Descripcion...">
				</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="fecha_inicio">Fecha de inicio</label>
					<input type="date" name="fecha_inicio"  class="form-control">
				</div>
			</div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="estado">Seleccione el estado de su tarea</label><br>
					<select name="estado" class="selectpicker col-lg-12 col-md-12 col-sm-12 col-xs-12" data-live-search="true">
							<option  value="terminada">En proceso</option>
							<option  value="pospuesta">Pospuesta</option>
							<option  value="finalizada">Finalizada</option>
					</select>
				</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="fecha_finalizacion">Fecha de finalizacion</label>
					<input type="date" name="fecha_finalizacion"  class="form-control">
				</div>
			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>
					<button class="btn btn-danger" type="reset">Cancelar</button><br><br>
					<a class="btn" href="/home">Regresar</a>
				</div>
			</div>
			{!!Form::close()!!}
		</div>
	</div>
</div>
@endsection
