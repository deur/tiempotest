@extends('layouts.app')
@section('content')

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<center><h3>Hola {{ Auth::user()->name }}, este es el panel de tareas. Para iniciar, crear nuevas categorías. Presiona el botón Nueva categoría.</h3> </center>
				
			<a href="/categoria">
			<button class="btn btn-success">Nueva categoria</button>
			</a>
			<a href="home/create">
			<button class="btn btn-success">Nueva tarea</button>
			</a>
			<br><br>


	{!! Form::open(['route'=>'tareas', 'method' => 'GET',  'class' => 'form-inline '])!!}

	<div class="form-group">
	{!! Form::text('nombre', null,['class'=>'form-control','id'=>'nombre', 'placeholder'=>'Buscar tarea...', 'autocomplete'=>'off'])
		
	!!} 	
	<div class="form-group" id="nombreList">
			    	{{ csrf_field() }}
	</div>
	</div>

	{!! Form::close()!!}
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table  table-hover table-striped " data-toggle="table"  data-show-refresh="true" data-show-toggle="true"  data-pagination="true" data-filter-control="true" data-show-columns="true"  data-search="true"   data-show-header="true" data-strict-search="false"  >
					<thead>
						<th >Nombre</th>
						<th >Categoría</th>
						<th>Descripción </th>
						<th>fecha de inicio </th>
						<th>Estado</th>
						<th >Fecha de finalizacion</th>
						<th>Acciones</th>
					</thead>
					@foreach ($tareas as $tar)
					<tr>
						<td>{{$tar->nombre}}</td>
						<td>{{$tar->categoria}}</td>
						<td>{{$tar->descripcion}}</td>  
						<td>{{$tar->fecha_inicio}}</td>
						<td>{{$tar->estado}}</td>
						<td>{{$tar->fecha_finalizacion}}</td>
						<!--Botones-->
						<td><center>
						<a href="{{URL::action('TareaController@edit',$tar->id)}}"><i class="fas fa-edit"></i></a>
						<a  href="{{URL::to('/tarea/del/'.$tar->id)}}" onclick="return confirm('Estás seguro de eliminar la tarea {{$tar->nombre}}')"><i class="fas fa-trash-alt"></i></a>
						</center></td>	
					</tr>
					
					@endforeach
				</table>
			</div>
			{{$tareas->render()}}
		</div>
	</div>
@endsection

