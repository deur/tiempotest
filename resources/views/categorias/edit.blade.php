@extends('layouts.app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<!--Si no tengo errores se cuentan, es decir que si hay más de 0 errores asigno una alerta-->
			@if(count($errors)>0)
			<!--Voy a mostrar las alertas de las validaciones propuestas en CategoriaFormRequest -->
			<div class="alert alert-danger">
				<ul>
					<!--Bucle que recibe todos los errores de las validaciones y que se va a listar -->
					@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
					@endforeach
				</ul>
			</div>
			@endif
			<!--Luego de hacer las validaciones si enviar los parámetros-->
			{!!Form::model($categoria,['method'=>'PATCH','route'=>['categoria.update',$categoria->id_categoria],'files'=>'true'])!!}
			{{Form::token()}}
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label for="nombre">Nombre la categoría</label>
					<input type="text" name="nombre" required value="{{$categoria->nombre}}" class="form-control" >
				</div>
			</div>              

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Guardar</button>
					<button class="btn btn-danger" type="reset">Cancelar</button>
					<br><br>
					<a class="btn" href="/categoria">Regresar</a>
				</div>
			</div>
			{!!Form::close()!!}
		</div>
	</div>
</div>
@endsection
