@extends('layouts.app')
@section('content')

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<center><h3>Listado de categorías </h3> </center>

			<a href="/categoria/create">
			<button class="btn btn-success">Nueva categoría</button>
			</a>
			 <a href="/home">
			 <button class="btn btn-success">Volver al menú</button>
			 </a>

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table  table-hover table-striped " data-toggle="table"  data-show-refresh="true" data-show-toggle="true"  data-pagination="true" data-filter-control="true" data-show-columns="true"  data-search="true"  data-show-header="true" data-strict-search="true"  >
					<thead>
						<th >Categoría</th>
						<th>Acciones</th>
					</thead>
					@foreach ($categorias as $cat)
					<tr>
						<td>{{$cat->nombre}}</td>
						<!--Botones-->
						<td><center>
						<a href="{{URL::action('CategoriaController@edit',$cat->id_categoria)}}"><i class="fas fa-edit"></i></a>
                        <a  href="{{URL::to('/categoria/del/'.$cat->id_categoria)}}" onclick="return confirm('Estás seguro de eliminar la tarea {{$cat->nombre}}')"><i class="fas fa-trash-alt"></i></a>
						</center></td>	
					</tr>
					@endforeach
				</table>
		</div>
		{{$categorias->render()}}
	</div>
</div>
@endsection