<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Tareas</title>



    <!-- Custom styles for this template -->
    <link href="{{asset('/css/cover.css')}}" rel="stylesheet">

        <!-- Styles -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i" rel="stylesheet">


        <style>


            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }



            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

                <header class="masthead mb-auto">
                    @if (Route::has('login'))
                        <div class="top-right links">
                            @auth
                                <a href="{{ url('/home') }}">Inicio</a>
                            @else
                                <a href="{{ route('login') }}">Entrar</a>

                                @if (Route::has('register'))
                                    <a href="{{ route('register') }}">Regístrate</a>
                                @endif
                            @endauth
                        </div>
                    @endif
                    </header>

                    <main role="main" class="inner cover">
                        <h1 class="cover-heading">Aplicación Tareas.</h1>
                        <p class="lead">Accede y gestiona tus tareas de una manera simple y descomplicada.</p>
                        
                    </main>

        </div>
    </body>
    
</html>
