<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect; //para redirecciones
use App\Http\Requests\CategoriaFormRequest;
use App\Categoria;
use DB; //Clase DB laravel ---CRUD---
class CategoriaController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
   public function index(Request $request){
        
    if ($request) 
    {   
        $query=trim($request->get('nombre'));
        $categorias=DB::table('categoria') 
        ->where('nombre','LIKE','%'.$query.'%')
        ->select('categoria.*')
        ->orderBy('categoria.id_categoria', 'desc')
        ->paginate(10);
        return view('categorias.index', ["categorias"=>$categorias, "nombre"=>$query]);

        } 
    }

    public function fetch(Request $request)
    {
     if($request->get('query'))
     {
      $query = $request->get('query');
      $data = DB::table('tarea')
        ->where('nombre', 'LIKE', "%{$query}%")
        ->get();
      $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
      foreach($data as $row)
      {
       $output .= '<li><option>'.$row->nombre.'</option></li>';
      }
      $output .= '</ul>';
      echo $output;
     }
    }
    
    public function create()
    {
    return view('categorias.create');
    }

    


        public function store(CategoriaFormRequest $request)
        {
            $categoria= new Categoria;
            $categoria->nombre=   $request->get('nombre');
            $categoria->usuario_id = auth()->id();
            $categoria->save();
            
            return Redirect::to('/categoria')->with('message','store');
        }

        public function show($id_categoria)
        {
            return view("categoria.show", ["categoria"=>Categoria::findOrFail($id_categoria)]);
        }

        public function edit($id_categoria)
        {
            $categoria=Categoria::findOrFail($id_categoria);
            return view("categorias.edit", ["categoria"=>$categoria]);
        }

        public function update(CategoriaFormRequest $request, $id_categoria)
    {
        $categoria=Categoria::findOrFail($id_categoria);
        $categoria->nombre=   $request->get('nombre');
        $categoria->usuario_id = auth()->id();
        $categoria->update();
        return Redirect::to('/categoria');
    }
    public function destroy($id_categoria)
    {
        $categoria=Categoria::findOrFail($id_categoria);
        $categoria->delete($id_categoria);
        return Redirect::to('categoria');
    }
}