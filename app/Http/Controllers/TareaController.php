<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect; //para redirecciones
use App\Http\Requests\TareaFormRequest;
use App\Tarea;
use App\Categoria;
use DB; //Clase DB laravel ---CRUD---
class TareaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request){
        
        if ($request) 
        {   
            $query=trim($request->get('nombre'));
            $tareas=DB::table('tarea as t') 
            ->where('t.nombre','LIKE','%'.$query.'%')
            ->join('categoria as c', 't.categoria_id', '=', 'c.id_categoria')
            ->select('t.*', 'c.nombre as categoria')
            ->orderBy('t.id', 'desc')
            ->paginate(200);
            return view('tareas.index', ["tareas"=>$tareas, "nombre"=>$query], compact('tareas'));
  
        } 
    }

    public function autocomplete(Request $request)
    {
     if($request->get('query'))
     {
      $query = $request->get('query');
      $data = DB::table('tarea')
        ->where('nombre', 'LIKE', "%{$query}%")
        ->get();
      $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
      foreach($data as $row)
      {
       $output .= '<li><option>'.$row->nombre.'</option></li>';
      }
      $output .= '</ul>';
      echo $output;
     }
    }

    public function create()
    {
        
        $categorias=DB::table('categoria')->get();


        return view('tareas.create', compact('categorias'));
    }  


    public function store(TareaFormRequest $request)
    {
        $tarea= new Tarea;
        $tarea->nombre=              $request->get('nombre');
        $tarea->categoria_id=        $request->get('categoria_id');
        $tarea->descripcion=         $request->get('descripcion');
        $tarea->fecha_inicio=        $request->get('fecha_inicio');
        $tarea->estado=              $request->get('estado');
        $tarea->fecha_finalizacion=  $request->get('fecha_finalizacion');
        $tarea->usuario_id =     auth()->id();
        $tarea->save();
        return Redirect::to('home')->with('message','store');
    }

    public function edit($id)
    {
        $tarea=Tarea::findOrFail($id);
        $categorias=DB::table('categoria')->get();

        return view("tareas.edit", ["tarea"=>$tarea, "categorias"=>$categorias]);
    }

    public function update(TareaFormRequest $request, $id)
    {
        $tarea= Tarea::findOrFail($id);
        $tarea->nombre=              $request->get('nombre');
        $tarea->categoria_id=        $request->get('categoria_id');
        $tarea->descripcion=         $request->get('descripcion');
        $tarea->fecha_inicio=        $request->get('fecha_inicio');
        $tarea->estado=              $request->get('estado');
        $tarea->fecha_finalizacion=  $request->get('fecha_finalizacion');
        $tarea->usuario_id =     auth()->id();
        $tarea->update();
        return Redirect::to('/home');
    }

    public function destroy($id)
    {
        $tarea=Tarea::findOrFail($id);
        $tarea->delete();
        return Redirect::to('home');
    }


}
 