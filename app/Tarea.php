<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    protected $table = 'tarea';
    protected $primaryKey='id';
    public $timestamps=false;

    protected $fillable =[
        'nombre',
        'categoria_id',
        'descripcion',
        'fecha_inicio',
        'estado',
        'fecha_finalizacion',
        'usuario_id '
    ];
    public function scopeSearch($query, $nombre){
        return $query->where('nombre', 'LIKE', "%nombre%");
    }

}
