<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::resource('home', 'TareaController');
Route::get('home', 'TareaController@index')->name('tareas');
Route::get('tarea/del/{id?}','TareaController@destroy');
//Apartado autocompletar
Route::post('autocomplete','TareaController@autocomplete')->name('autocomplete');
// Gestionar Apartado categorias
Route::resource('categoria', 'CategoriaController');
Route::get('categoria','CategoriaController@index')->name('categorias');
Route::get('categoria/del/{id_categoria?}','CategoriaController@destroy');
